// measure intensity

input = getArgument();
suffix = ".tif";

processFolder(input);

function processFolder(input) {
	run("Input/Output...", "jpeg=85 gif=-1 file=.csv use_file copy_row save_column");
	run("Set Measurements...", "area mean standard modal perimeter shape integrated area_fraction limit display add redirect=None decimal=3");
	
	list = getFileList(input);
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input + list[i]))
			processFolder("" + input + list[i]);
		if(endsWith(list[i], suffix))
			processFile(input, list[i]);
	}
}

function processFile(input, output, file) {
	open(input+file);
	run("Measure");
}

saveAs("Results", input+"intensity.csv");
print("Saved to: " + output);

run("Quit");
