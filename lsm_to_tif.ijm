// Script converts .lsm files to .tif files

input = getArgument();
output = input

print("Input: "+input);
print("Output: "+output);

suffix = ".lsm";

processFolder(input);

function processFolder(input) {
	list = getFileList(input);
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input + list[i]))
			processFolder("" + input + list[i]);
		if(endsWith(list[i], suffix))
			processFile(input, output, list[i]);
	}
}

function processFile(input, output, file) {
	open(input+file);
    file_path = output + replace(getTitle(), ".lsm", ".tif");
    saveAs("Tiff", file_path);
    close();
}

print("Saved .tif files to: " + output);
run("Quit");
